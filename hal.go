package hal

import (
	"encoding/json"
)

// Res is a HAL compliant Resource
type Res struct {
	Data map[string]interface{}
}

type ToRes interface {
	ToRes() *Res
}

// MSI is a help type so you can type `hal.MSI` instead of map[string]interface
type MSI map[string]interface{}

// ToRes implements the ToRes interface needed to Embed a Res
func (r *Res) ToRes() *Res {
	return r
}

// New creates a new Res with a self rel that has an href as an attribute
func New(selfHref string) *Res {
	res := new(Res)

	res.AddLink("self", map[string]interface{}{
		"href": selfHref,
	})

	return res
}

func (r *Res) json(pretty bool) (string, error) {
	// Init
	var j []byte
	var err error

	if pretty {
		j, err = json.MarshalIndent(r.Data, "", "\t")
	} else {
		j, err = json.Marshal(r.Data)
	}
	if err != nil {
		return "", err
	}

	return string(j), nil
}

// JSON returns a JSON representation of the Res
func (r *Res) JSON() (string, error) {
	return r.json(false)
}

// Pretty returns a pretty printed JSON representation of the Res
func (r *Res) Pretty() (string, error) {
	return r.json(true)
}

// AddLink adds a link to the Res
func (r *Res) AddLink(rel string, value map[string]interface{}) {
	if r.Data == nil {
		r.Data = make(map[string]interface{})
	}
	if r.Data["_links"] == nil {
		r.Data["_links"] = make(map[string]interface{})
	}

	links := r.Data["_links"].(map[string]interface{})
	links[rel] = value
	r.Data["_links"] = links
}

// AddItems adds items to the Res
func (r *Res) AddItems(items map[string]interface{}) {
	if r.Data == nil {
		r.Data = make(map[string]interface{})
	}

	for k, v := range items {
		r.Data[k] = v
	}
}

// AddItem adds a single Item to the Res
func (r *Res) AddItem(key string, value interface{}) {
	r.Data[key] = value
}

// Embed embeds a resource into the Res
func (r *Res) Embed(rel string, trs ...ToRes) {
	if r.Data == nil {
		r.Data = make(map[string]interface{})
	}
	if r.Data["_embedded"] == nil {
		r.Data["_embedded"] = make(map[string][]map[string]interface{})
	}

	for _, tr := range trs {
		res := tr.ToRes()
		ress := r.Data["_embedded"].(map[string][]map[string]interface{})
		ress[rel] = append(ress[rel], res.Data)
		r.Data["_embedded"] = ress
	}
}
