package hal_test

import (
	"bitbucket.org/jmaloney/hal"
	"fmt"
	"log"
)

func ExampleNew() {
	res := hal.New("http://selflink.com")

	r, err := res.Pretty()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Print(r)
	// Output:
	//{
	//	"_links": {
	//		"self": {
	//			"href": "http://selflink.com"
	//		}
	//	}
	//}
}

func ExampleRes_AddLink() {
	res := hal.New("http://selflink.com")

	res.AddLink("next", hal.MSI{
		"href":      "http://selflink.com?next",
		"templated": true,
	})

	r, err := res.Pretty()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Print(r)
	// Output:
	//{
	//	"_links": {
	//		"next": {
	//			"href": "http://selflink.com?next",
	//			"templated": true
	//		},
	//		"self": {
	//			"href": "http://selflink.com"
	//		}
	//	}
	//}
}

func ExampleRes_AddItem() {
	res := hal.New("http://selflink.com")

	// Add simple item
	res.AddItem("foo", "bar")

	// Add a more complex item
	res.AddItem("things", hal.MSI{
		"Lamp": hal.MSI{
			"voltage": 120,
			"watage":  "60W",
		},
		"Coffee": hal.MSI{
			"roast":  "mediam",
			"region": "Hawaii",
		},
	})

	// Add an array
	res.AddItem("candy", []string{
		"Snickers",
		"Gummies",
		"Brix",
		"Dots",
	})

	r, err := res.Pretty()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Print(r)
	// Output:
	//{
	//	"_links": {
	//		"self": {
	//			"href": "http://selflink.com"
	//		}
	//	},
	//	"candy": [
	//		"Snickers",
	//		"Gummies",
	//		"Brix",
	//		"Dots"
	//	],
	//	"foo": "bar",
	//	"things": {
	//		"Coffee": {
	//			"region": "Hawaii",
	//			"roast": "mediam"
	//		},
	//		"Lamp": {
	//			"voltage": 120,
	//			"watage": "60W"
	//		}
	//	}
	//}
}

func ExampleRes_Embed() {
	parent := hal.New("http://selflink.com")
	childOne := hal.New("http://childOne.com")
	childTwo := hal.New("http://childTwo.com")

	parent.Embed("children", childOne, childTwo)

	r, err := parent.Pretty()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Print(r)

	// Output:
	//{
	//	"_embedded": {
	//		"children": [
	//			{
	//				"_links": {
	//					"self": {
	//						"href": "http://childOne.com"
	//					}
	//				}
	//			},
	//			{
	//				"_links": {
	//					"self": {
	//						"href": "http://childTwo.com"
	//					}
	//				}
	//			}
	//		]
	//	},
	//	"_links": {
	//		"self": {
	//			"href": "http://selflink.com"
	//		}
	//	}
	//}
}
